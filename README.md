# CarDB

Provides a sample sqlite database file.

SQLite Editor can be downloaded from:
*  [https://sqlitebrowser.org/dl/](https://sqlitebrowser.org/dl/) (Recommended: DB Browser for SQLite - .zip (no installer) for 64-bit Windows)

How to edit database:
* Execute *DB Browser for SQLite.exe*
* Open Database -> select *cardb.sqlite*
* In *Browse Data* tab, under Table *Car*, edit the entries or add new record with *New Record* button
  * Column *id* should be the unique car id
  * Column *info1* ~ *info4* are optional to be displayed
* *Write Changes* to save changes